//
//  Utils.swift
//  HummingbirdChallenge
//
//  Created by Gabriel Moraes on 4/23/17.
//  Copyright © 2017 Gabriel Moraes. All rights reserved.
//

import UIKit

typealias JSON = [String: Any]

struct Constants {
    
    static let apiKey = "a956bae9af2dc35147155120dc83cc48"
    
    static let urlBase = URLComponents(string: "https://api.themoviedb.org/")!
    
    private static let apiVersion = "/3"
    static let pathPopular = apiVersion + "/movie/popular"
    static let pathSearch = apiVersion + "/search/movie"
    static let pathConfiguration = apiVersion + "/configuration"
    
    static let dateFormatter = DateFormatter(dateFormat: "yyyy/MM/dd")

}

extension DateFormatter {
    convenience init(dateFormat: String) {
        self.init()
        self.dateFormat = dateFormat
    }
}

extension Date {
    func getYear() -> String {
        let dateFormatter = DateFormatter(dateFormat: "yyyy")
        return dateFormatter.string(from: self)
    }
}

let imageCache = NSCache<AnyObject, AnyObject>()

extension UIImageView {
    func loadImage(with urlString: String) {
        self.image = nil
        if let cachedImage = imageCache.object(forKey: urlString as AnyObject) as? UIImage {
            self.image = cachedImage
            return
        }
        let url = URL(string: urlString)!
        URLSession.shared.dataTask(with: url) { data, response, error in
            DispatchQueue.main.async {
                if let imageData = data {
                    let image = UIImage(data: imageData)!
                    self.image = image
                    imageCache.setObject(image, forKey: urlString as AnyObject)
                } else {
                    self.image = nil
                }
            }
        }.resume()
    }
}
