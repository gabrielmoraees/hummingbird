//
//  Configuration.swift
//  HummingbirdChallenge
//
//  Created by Gabriel Moraes on 4/23/17.
//  Copyright © 2017 Gabriel Moraes. All rights reserved.
//

import UIKit

struct Configuration {
    
    let urlBase: String
    let posterSize: String
    
    init?(json: JSON) {
        guard let urlBase = json["secure_base_url"] as? String,
            let posterSizes = json["poster_sizes"] as? [String],
            let posterSize = posterSizes.first
        else {
            return nil
        }
        self.urlBase = urlBase
        self.posterSize = posterSize
    }
}

extension Configuration {
    static func current(completion: @escaping (Configuration?) -> Void) {
        var urlComponents = Constants.urlBase
        urlComponents.path = Constants.pathConfiguration
        urlComponents.queryItems = [
            URLQueryItem(name: "api_key", value: Constants.apiKey)
        ]
        URLSession.shared.dataTask(with: urlComponents.url!) { data, response, error in
            if let data = data,
                let json = try? JSONSerialization.jsonObject(with: data, options: []) as? JSON,
                let result = json?["images"] as? JSON,
                let configuration = Configuration(json: result) {
                DispatchQueue.main.async {
                    completion(configuration)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil)
                }
            }
        }.resume()
    }
}
