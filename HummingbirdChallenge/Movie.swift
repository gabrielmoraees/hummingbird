//
//  Movie.swift
//  HummingbirdChallenge
//
//  Created by Gabriel Moraes on 4/23/17.
//  Copyright © 2017 Gabriel Moraes. All rights reserved.
//

import Foundation

struct Movie {

    let id: Int
    let title: String
    let overview: String
    let posterPath: String
    let releaseDate: Date
    
    init?(json: JSON) {
        guard let id = json["id"] as? Int,
            let title = json["title"] as? String,
            let overview = json["overview"] as? String,
            let posterPath = json["poster_path"] as? String,
            let releaseDateString = json["release_date"] as? String,
            let releaseDate = Constants.dateFormatter.date(from: releaseDateString)
        else {
            return nil
        }
        self.id = id
        self.title = title
        self.overview = overview
        self.posterPath = posterPath
        self.releaseDate = releaseDate
    }
}

extension Movie {
    
    typealias MovieCompletionHandler = ([Movie], Int) -> Void
    
    static func movies(at page: Int, matching query: String? = nil, completion: @escaping MovieCompletionHandler) {
        var urlComponents = Constants.urlBase
        urlComponents.queryItems = [
            URLQueryItem(name: "api_key", value: Constants.apiKey),
            URLQueryItem(name: "page", value: page.description)
        ]
        let path: String
        if let queryString = query {
            let queryItem = URLQueryItem(name: "query", value: queryString)
            urlComponents.queryItems?.append(queryItem)
            path = Constants.pathSearch
            URLSession.shared.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in
                for task in dataTasks {
                    task.cancel()
                }
            }
        } else {
            path = Constants.pathPopular
        }
        urlComponents.path = path
        self.requestMovies(with: urlComponents.url!, completion: completion)
    }
    
    private static func requestMovies(with url: URL, completion: @escaping MovieCompletionHandler) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            var movies = [Movie]()
            var pages = Int.max
            if let data = data,
                let json = try? JSONSerialization.jsonObject(with: data, options: []) as? JSON,
                let results = json?["results"] as? [JSON],
                let totalPages = json?["total_pages"] as? Int {
                pages = totalPages
                for result in results {
                    if let movie = Movie(json: result) {
                        movies.append(movie)
                    }
                }
            }
            DispatchQueue.main.async {
                completion(movies, pages)
            }
        }.resume()
    }
    
}
