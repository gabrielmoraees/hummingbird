//
//  MovieTableViewCell.swift
//  HummingbirdChallenge
//
//  Created by Gabriel Moraes on 4/23/17.
//  Copyright © 2017 Gabriel Moraes. All rights reserved.
//

import UIKit

class MovieTableViewCell: UITableViewCell {

    @IBOutlet weak var imgPoster: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblYear: UILabel!
    @IBOutlet weak var lblOverview: UILabel!
    
    var urlBaseImage: String?
    
    var movie: Movie! {
        didSet {
            self.lblTitle.text = movie.title
            self.lblOverview.text = movie.overview
            self.lblYear.text = movie.releaseDate.getYear()
            if let urlBase = self.urlBaseImage {
                self.imgPoster.loadImage(with: urlBase + movie.posterPath)
                self.setNeedsLayout()
            } else {
                Configuration.current { [weak self] configuration in
                    if let currentConfig = configuration,
                        let path = self?.movie.posterPath {
                        let urlBase = currentConfig.urlBase + currentConfig.posterSize
                        self?.imgPoster.loadImage(with: urlBase + path)
                        self?.urlBaseImage = urlBase
                        self?.setNeedsLayout()
                    }
                }
            }
        }
    }

}
