//
//  MovieTableViewController.swift
//  HummingbirdChallenge
//
//  Created by Gabriel Moraes on 4/23/17.
//  Copyright © 2017 Gabriel Moraes. All rights reserved.
//

import UIKit

class MovieTableViewController: UITableViewController {
    
    let searchController = UISearchController(searchResultsController: nil)
    
    lazy var movies = [Movie]()
    lazy var loading = UIRefreshControl()
    
    var currentPage = 1
    var totalPages = Int.max
    var currentSearchPage = 1
    var totalSearchPages = Int.max
    var isLoadingMore = false
    
    var filteredMovies: [Movie]?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureRefreshControl()
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 154
        self.configureSearchController()
        self.popularMovies()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func reload() {
        if self.filteredMovies != nil {
            self.currentSearchPage = 1
            self.searchMovies()
        } else {
            self.currentPage = 1
            self.popularMovies()
        }
    }
    
    fileprivate func configureRefreshControl() {
        self.loading.addTarget(self, action: #selector(reload), for: .valueChanged)
        self.tableView.insertSubview(self.loading, at: 0)
        self.loading.beginRefreshing()
    }
    
    fileprivate func configureSearchController() {
        self.edgesForExtendedLayout = .top
        self.extendedLayoutIncludesOpaqueBars = true
        self.definesPresentationContext = true
        self.searchController.dimsBackgroundDuringPresentation = false
        self.searchController.searchResultsUpdater = self
        self.searchController.searchBar.delegate = self
        self.searchController.searchBar.sizeToFit()
        self.tableView.tableHeaderView = self.searchController.searchBar
    }
    
    fileprivate func popularMovies(loadMore: Bool = false) {
        Movie.movies(at: self.currentPage) { [weak self] movies, totalPages in
            self?.loading.endRefreshing()
            self?.isLoadingMore = false
            if loadMore {
                self?.movies.append(contentsOf: movies)
            } else {
                self?.movies = movies
                self?.totalPages = totalPages
            }
            self?.tableView.reloadData()
        }
    }
    
    fileprivate func searchMovies(loadMore: Bool = false) {
        if let query = self.searchController.searchBar.text, !query.isEmpty {
            Movie.movies(at: self.currentSearchPage, matching: query) { [weak self] movies, totalPages in
                self?.loading.endRefreshing()
                self?.isLoadingMore = false
                if self?.filteredMovies == nil {
                    self?.filteredMovies = []
                }
                if loadMore {
                    self?.filteredMovies?.append(contentsOf: movies)
                } else {
                    self?.filteredMovies = movies
                    self?.totalSearchPages = totalPages
                }
                self?.tableView.reloadData()
            }
        } else {
            self.resetSearch()
        }
    }
    
    fileprivate func resetSearch() {
        self.currentSearchPage = 1
        self.filteredMovies = nil
        self.tableView.reloadData()
    }
    
    //MARK: Scrollview delegate
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.tableView.contentOffset.y >= self.tableView.contentSize.height - self.tableView.frame.size.height
            && !self.isLoadingMore
            && !self.loading.isRefreshing {
            if self.filteredMovies != nil {
                if self.currentSearchPage < self.totalSearchPages {
                    self.isLoadingMore = true
                    self.currentSearchPage += 1
                    self.searchMovies(loadMore: true)
                }
            } else {
                if self.currentPage < self.totalPages {
                    self.isLoadingMore = true
                    self.currentPage += 1
                    self.popularMovies(loadMore: true)
                }
            }
        }
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let filteredCount = self.filteredMovies?.count else {
            return self.movies.count
        }
        return filteredCount
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MovieCell", for: indexPath) as! MovieTableViewCell
        
        let movie: Movie
        if let filteredMovies = self.filteredMovies {
            movie = filteredMovies[indexPath.item]
        } else {
            movie = self.movies[indexPath.item]
        }
        cell.movie = movie
        
        return cell
    }

}

extension MovieTableViewController: UISearchResultsUpdating, UISearchBarDelegate {
    
    func updateSearchResults(for searchController: UISearchController) {
        self.currentSearchPage = 1
        self.searchMovies()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.resetSearch()
    }
    
}
