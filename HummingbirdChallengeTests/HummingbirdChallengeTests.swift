//
//  HummingbirdChallengeTests.swift
//  HummingbirdChallengeTests
//
//  Created by Gabriel Moraes on 4/23/17.
//  Copyright © 2017 Gabriel Moraes. All rights reserved.
//

import XCTest
@testable import HummingbirdChallenge

class HummingbirdChallengeTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    func testPopularMovies() {
        let expect = expectation(description: "Task should succeed")
        Movie.movies(at: 1) { movies, totalPages in
            XCTAssertEqual(movies.count, 20, "Popular movies are not ready to paginate.")
            expect.fulfill()
        }
        waitForExpectations(timeout: 3.0) { error in
            XCTAssertNil(error, "Test timed out. \(error?.localizedDescription ?? "")")
        }
    }
    
    func testMoviesSearch() {
        let expect = expectation(description: "Task should succeed")
        Movie.movies(at: 1, matching: "Fight Club") { movies, totalPages in
            XCTAssertFalse(movies.isEmpty, "This great movie wasn't found. :(")
            expect.fulfill()
        }
        waitForExpectations(timeout: 3.0) { error in
            XCTAssertNil(error, "Test timed out. \(error?.localizedDescription ?? "")")
        }
    }
    
    func testConfiguration() {
        let expect = expectation(description: "Task should succeed")
        Configuration.current { configuration in
            XCTAssertNotNil(configuration, "Configuration couldn't be loaded.")
            expect.fulfill()
        }
        waitForExpectations(timeout: 3.0) { error in
            XCTAssertNil(error, "Test timed out. \(error?.localizedDescription ?? "")")
        }
    }
    
    func testGetYear() {
        let date = Constants.dateFormatter.date(from: "2017-04-23")
        XCTAssertNotNil(date, "Parsing failed.")
        XCTAssertEqual(date?.getYear(), "2017", "Parsing bug.")
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
}
